# Proj0-Hello
-------------

Simple Hello World program to demonstrate proper version control, turn-in, etc.

## Credits
-------------

-This program was orignially written by University of Oregon CIS Department all credit goes to them.

-Updated per assignment instructions by

Kendell Crocker
kendellc@uoregon.edu

## Instructions:
---------------

This program was designed to be uploaded to the forked repository for this assignment and then be graded by a bot. Here's how to run this program:

-Download the files from the repository, https://kcskier@bitbucket.org/kcskier/proj0-hello

-create a new file in the hello directory called "credentials.ini"

-Paste the following text into the file you just created and fill in the fields with your information:

```
[DEFAULT] 
author= Kendell Crocker
repo=https://bitbucket.org/kcskier/proj0-hello/src/master/
message=Hello world
```

-After saving credentials.ini the hello.py file should run perfectly!